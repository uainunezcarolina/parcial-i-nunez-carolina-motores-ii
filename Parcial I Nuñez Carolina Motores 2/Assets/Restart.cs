using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{

    void EscenaActual()
    {
        string escenaActual = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(escenaActual);

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            EscenaActual();
        }
    }
}
