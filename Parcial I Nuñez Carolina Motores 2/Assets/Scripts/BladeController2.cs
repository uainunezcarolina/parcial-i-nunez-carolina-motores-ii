using UnityEngine;

public class BladeController2 : MonoBehaviour
{
    public float moveDistance = 5.0f; // Distancia que se mover� la sierra
    public float moveSpeed = 1.0f; // Velocidad a la que se mueve la sierra
    public float rotationSpeed = 200.0f; // Velocidad de rotaci�n de la sierra
    public float damage = 10.0f; // Da�o que hace la sierra al jugador

    private bool movingUp = true; // Controla la direcci�n de movimiento
    private float moveStartTime; // Tiempo en el que comenz� el movimiento
    private Vector3 startPosition; // Posici�n de inicio del movimiento
    private Vector3 endPosition; // Posici�n de finalizaci�n del movimiento
    private AnimationCurve moveCurve; // Curva de movimiento de la sierra

    void Start()
    {
        // Almacenar la posici�n de inicio y finalizaci�n del movimiento
        startPosition = transform.position;
        endPosition = transform.position + new Vector3(0, moveDistance, 0);

        // Crear una curva de movimiento utilizando un AnimationCurve
        moveCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        moveCurve.postWrapMode = WrapMode.PingPong;
    }

    void Update()
    {
        // Obtener el tiempo transcurrido desde el inicio del movimiento
        float timeSinceStart = Time.time - moveStartTime;

        // Obtener la posici�n actual de la sierra utilizando la curva de movimiento
        float curveValue = moveCurve.Evaluate(timeSinceStart * moveSpeed);
        Vector3 currentPosition = Vector3.Lerp(startPosition, endPosition, curveValue);

        // Actualizar la posici�n de la sierra
        transform.position = currentPosition;

        // Girar la sierra
        float rotationAmount = rotationSpeed * Time.deltaTime;
        transform.Rotate(Vector3.forward, rotationAmount);

        // Cambiar la direcci�n de movimiento si se ha llegado al final del recorrido
        if (movingUp && currentPosition.y >= endPosition.y || !movingUp && currentPosition.y <= startPosition.y)
        {
            movingUp = !movingUp;
            moveStartTime = Time.time;
        }
    }


}
