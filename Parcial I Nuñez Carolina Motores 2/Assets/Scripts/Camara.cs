using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    public Transform target; // Referencia al Transform del jugador
    public float smoothSpeed = 0.125f; // Velocidad de suavizado para el movimiento de la c�mara

    private Vector3 offset; // Distancia inicial entre la c�mara y el jugador

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - target.position; // Calcula la distancia inicial entre la c�mara y el jugador

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 desiredPosition = target.position + offset; // Calcula la posici�n deseada de la c�mara basada en la posici�n actual del jugador
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed); // Aplica un suavizado al movimiento de la c�mara

        transform.position = smoothedPosition; // Actualiza la posici�n de la c�mara
    }
}
