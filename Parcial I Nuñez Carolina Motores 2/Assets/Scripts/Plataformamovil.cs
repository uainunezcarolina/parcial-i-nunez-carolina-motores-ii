using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataformamovil : MonoBehaviour
{

    public float speed = 5;
    public float altmaxima = 3;
    public float altminima = 1;      

    private bool movingup = true;

    // Update is called once per frame
    void Update()
    {
        if (movingup)
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        } else
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);
        }

        if (transform.position.y >= altmaxima)
        {
            movingup = false;
        }
        else if (transform.position.y <= altminima)
        {
            movingup = true;
        }

    }
}
