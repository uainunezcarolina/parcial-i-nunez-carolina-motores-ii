using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataformacostados : MonoBehaviour
{
    public float speed = 5f; // Velocidad de movimiento de la plataforma
    public float minX = 1f; // Posici�n X m�nima
    public float maxX = 3f; // Posici�n X m�xima

    private bool movingRight = true; // Bandera para determinar la direcci�n de movimiento

    void Update()
    {
        if (movingRight)
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }

        if (transform.position.x >= maxX)
        {
            movingRight = false;
        }
        else if (transform.position.x <= minX)
        {
            movingRight = true;
        }
    }
}
