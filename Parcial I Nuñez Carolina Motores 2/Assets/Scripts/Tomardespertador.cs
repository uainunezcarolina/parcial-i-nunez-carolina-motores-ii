using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tomardespertador : MonoBehaviour
{
    private void OnTriggerEnter (Collider collision)
    {
        if (collision.gameObject.CompareTag("Despertador"))
        {
            Cambioescena();

        }
    }

    public void Cambioescena()
    {
        int siguienteEscena = SceneManager.GetActiveScene().buildIndex + 1;
        SceneManager.LoadScene(siguienteEscena);

    }

    public void exit()
    {
        Application.Quit();
    }

}
