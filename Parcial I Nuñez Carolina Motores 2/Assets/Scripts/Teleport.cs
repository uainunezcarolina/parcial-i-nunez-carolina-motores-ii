using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform destino;
    Movimiento jugador;

    public void Start()
    {
        jugador= FindObjectOfType<Movimiento>();
    }
    public void activarTeleport()
    {
        Vector3 playerposition = jugador.transform.position;
        jugador.transform.position= destino.position;
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            activarTeleport();
        }

    }
}
