using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour
{
    public float speed = 5f; // Velocidad de movimiento del jugador
    public float jumpForce = 5f; // Fuerza de salto del jugador
    private bool isJumping = false;
    private bool puerta = false;

    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector2 movement = new Vector2(moveHorizontal, 0f);
        rb.velocity = new Vector2(movement.x * speed, rb.velocity.y); // Aplicar la velocidad horizontal al cuerpo r�gido del jugador
        if (Input.GetKeyDown(KeyCode.W)&& !isJumping) // Verificar si el jugador est� en el suelo y se presion� el bot�n de salto
        {
            Jump();

        }
        if (puerta)
        {
            Destroy(GameObject.FindGameObjectWithTag("Puerta"));
        }

    }
    private void Jump()
    {
        isJumping = true;
        rb.AddForce(Vector3.up*jumpForce,ForceMode.Impulse); // Aplicar una fuerza vertical para el salto
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Piso"))
        {
            isJumping = false;
        }

        string escenaActual = SceneManager.GetActiveScene().name;
        if (collision.gameObject.CompareTag("Kill"))
        {
            SceneManager.LoadScene(escenaActual);

        }

        if (collision.gameObject.CompareTag("Gelatina"))
        {
            speed = 2;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Gelatina"))
        {
            speed = 5;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Llave"))
        {
            puerta = true;
            Destroy(other.gameObject);
        }
    }

}
